.EXPORT_ALL_VARIABLES:

#=================================================================================================================================
# INITIAL ASSEMBLY SETUP
#=================================================================================================================================
ifneq ($(and $(project),$(stage)),)
$(shell ./prepare_project ${project} ${stage})
ENV_FILE=.env.${project}
include ${ENV_FILE}
export $(shell set -a && source ${ENV_FILE} && set +a)
else
ifndef project
$(info $(shell tput setaf 1)Project name is not set. For example $(shell tput sgr0)$(shell tput setaf 6)'project=citibank-rain'$(shell tput sgr0))
endif
ifndef stage
$(info $(shell tput setaf 1)Stage is not set. For example $(shell tput sgr0)$(shell tput setaf 6)'stage=local'$(shell tput sgr0))
endif
ERROR=true
endif

#=================================================================================================================================
# MAIN COMMANDS
#=================================================================================================================================
app:
ifndef ERROR
ifdef s
	echo ${COMPOSE_PROJECT_NAME}
	@docker-compose up -d --no-deps --build ${s}
else
	@docker-compose up -d --no-deps --build
endif
endif

down:
ifndef ERROR
	@docker-compose down
endif

exec:
ifndef ERROR
ifdef s
	@docker-compose exec ${s} bash
else
	exit 1
endif
endif

list:
ifndef ERROR
ifndef ERROR
	@docker-compose ps
endif
endif

log:
ifndef ERROR
ifdef s
	@docker-compose logs ${s}
else
	@docker-compose logs
endif
endif
