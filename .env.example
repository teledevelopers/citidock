###########################################################
###################### General Setup ######################
###########################################################
APP_NAME=${APP_NAME}

### Paths #################################################
# Point to the path of your applications code on your host
APP_CODE_PATH_HOST=${APP_CODE_PATH_HOST}
# Point to where the `APP_CODE_PATH_HOST` should be in the container
APP_CODE_PATH_CONTAINER=${APP_CODE_PATH_CONTAINER}
# You may add flags to the path `:cached`, `:delegated`. When using Docker Sync add `:nocopy`
APP_CODE_CONTAINER_FLAG=${APP_CODE_CONTAINER_FLAG}
# Choose storage path on your machine. For all storage systems
DATA_PATH_HOST=${DATA_PATH_HOST}

### Drivers ################################################
# All volumes driver
VOLUMES_DRIVER=${VOLUMES_DRIVER}
# All Networks driver and network config
NETWORKS_DRIVER=${NETWORKS_DRIVER}
NETWORKS_SUBNET=${NETWORKS_SUBNET}

### Docker compose files ##################################
# Select which docker-compose files to include. If using docker-sync append `:docker-compose.sync.yaml` at the end
COMPOSE_FILE=${COMPOSE_FILE}
# Change the separator from : to ; on Windows
COMPOSE_PATH_SEPARATOR=${COMPOSE_PATH_SEPARATOR}
# Define the prefix of container names. This is useful if you have multiple projects that use laradock to have separate containers per project.
COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME}

### PHP Version ###########################################
# Select a PHP version of the Workspace and PHP-FPM containers (Does not apply to HHVM).
# Accepted values: 8.0 - 7.4 - 7.3 - 7.2 - 7.1 - 7.0 - 5.6
PHP_VERSION=${PHP_VERSION}

### PHP Interpreter #######################################

# Select the PHP Interpreter. Accepted values: hhvm - php-fpm
PHP_INTERPRETER=${PHP_INTERPRETER}

### Remote Interpreter ####################################

# Choose a Remote Interpreter entry matching name. Default is `laradock`
PHP_IDE_CONFIG=${PHP_IDE_CONFIG}

### DOCKERREGISTRY ###############################################
DOCKER_REGISTRY_PORT=${DOCKER_REGISTRY_PORT}

### WORKSPACE #############################################
WORKSPACE_BASE_IMAGE_TAG_PREFIX=${WORKSPACE_BASE_IMAGE_TAG_PREFIX}
WORKSPACE_COMPOSER_GLOBAL_INSTALL=${WORKSPACE_COMPOSER_GLOBAL_INSTALL}
WORKSPACE_COMPOSER_VERSION=${WORKSPACE_COMPOSER_VERSION}
WORKSPACE_COMPOSER_AUTH=${WORKSPACE_COMPOSER_AUTH}
WORKSPACE_COMPOSER_REPO_PACKAGIST=${WORKSPACE_COMPOSER_REPO_PACKAGIST}
WORKSPACE_NVM_NODEJS_ORG_MIRROR=${WORKSPACE_NVM_NODEJS_ORG_MIRROR}
WORKSPACE_INSTALL_NODE=${WORKSPACE_INSTALL_NODE}
WORKSPACE_NODE_VERSION=${WORKSPACE_NODE_VERSION}
WORKSPACE_NPM_REGISTRY=${WORKSPACE_NPM_REGISTRY}
WORKSPACE_NPM_FETCH_RETRIES=${WORKSPACE_NPM_FETCH_RETRIES}
WORKSPACE_NPM_FETCH_RETRY_FACTOR=${WORKSPACE_NPM_FETCH_RETRY_FACTOR}
WORKSPACE_NPM_FETCH_RETRY_MINTIMEOUT=${WORKSPACE_NPM_FETCH_RETRY_MINTIMEOUT}
WORKSPACE_NPM_FETCH_RETRY_MAXTIMEOUT=${WORKSPACE_NPM_FETCH_RETRY_MAXTIMEOUT}
WORKSPACE_INSTALL_PNPM=${WORKSPACE_INSTALL_PNPM}
WORKSPACE_INSTALL_YARN=${WORKSPACE_INSTALL_YARN}
WORKSPACE_YARN_VERSION=${WORKSPACE_YARN_VERSION}
WORKSPACE_INSTALL_NPM_GULP=${WORKSPACE_INSTALL_NPM_GULP}
WORKSPACE_INSTALL_NPM_BOWER=${WORKSPACE_INSTALL_NPM_BOWER}
WORKSPACE_INSTALL_NPM_VUE_CLI=${WORKSPACE_INSTALL_NPM_VUE_CLI}
WORKSPACE_INSTALL_NPM_ANGULAR_CLI=${WORKSPACE_INSTALL_NPM_ANGULAR_CLI}
WORKSPACE_INSTALL_PHPREDIS=${WORKSPACE_INSTALL_PHPREDIS}
WORKSPACE_INSTALL_WORKSPACE_SSH=${WORKSPACE_INSTALL_WORKSPACE_SSH}
WORKSPACE_INSTALL_SUBVERSION=${WORKSPACE_INSTALL_SUBVERSION}
WORKSPACE_INSTALL_BZ2=${WORKSPACE_INSTALL_BZ2}
WORKSPACE_INSTALL_GMP=${WORKSPACE_INSTALL_GMP}
WORKSPACE_INSTALL_GNUPG=${WORKSPACE_INSTALL_GNUPG}
WORKSPACE_INSTALL_XDEBUG=${WORKSPACE_INSTALL_XDEBUG}
WORKSPACE_INSTALL_PCOV=${WORKSPACE_INSTALL_PCOV}
WORKSPACE_INSTALL_PHPDBG=${WORKSPACE_INSTALL_PHPDBG}
WORKSPACE_INSTALL_SSH2=${WORKSPACE_INSTALL_SSH2}
WORKSPACE_INSTALL_LDAP=${WORKSPACE_INSTALL_LDAP}
WORKSPACE_INSTALL_SOAP=${WORKSPACE_INSTALL_SOAP}
WORKSPACE_INSTALL_XSL=${WORKSPACE_INSTALL_XSL}
WORKSPACE_INSTALL_SMB=${WORKSPACE_INSTALL_SMB}
WORKSPACE_INSTALL_IMAP=${WORKSPACE_INSTALL_IMAP}
WORKSPACE_INSTALL_MONGO=${WORKSPACE_INSTALL_MONGO}
WORKSPACE_INSTALL_AMQP=${WORKSPACE_INSTALL_AMQP}
WORKSPACE_INSTALL_CASSANDRA=${WORKSPACE_INSTALL_CASSANDRA}
WORKSPACE_INSTALL_GEARMAN=${WORKSPACE_INSTALL_GEARMAN}
WORKSPACE_INSTALL_MSSQL=${WORKSPACE_INSTALL_MSSQL}
WORKSPACE_INSTALL_DRUSH=${WORKSPACE_INSTALL_DRUSH}
WORKSPACE_DRUSH_VERSION=${WORKSPACE_DRUSH_VERSION}
WORKSPACE_INSTALL_DRUPAL_CONSOLE=${WORKSPACE_INSTALL_DRUPAL_CONSOLE}
WORKSPACE_INSTALL_WP_CLI=${WORKSPACE_INSTALL_WP_CLI}
WORKSPACE_INSTALL_AEROSPIKE=${WORKSPACE_INSTALL_AEROSPIKE}
WORKSPACE_INSTALL_OCI8=${WORKSPACE_INSTALL_OCI8}
WORKSPACE_INSTALL_V8JS=${WORKSPACE_INSTALL_V8JS}
WORKSPACE_INSTALL_LARAVEL_ENVOY=${WORKSPACE_INSTALL_LARAVEL_ENVOY}
WORKSPACE_INSTALL_LARAVEL_INSTALLER=${WORKSPACE_INSTALL_LARAVEL_INSTALLER}
WORKSPACE_INSTALL_DEPLOYER=${WORKSPACE_INSTALL_DEPLOYER}
WORKSPACE_INSTALL_PRESTISSIMO=${WORKSPACE_INSTALL_PRESTISSIMO}
WORKSPACE_INSTALL_LINUXBREW=${WORKSPACE_INSTALL_LINUXBREW}
WORKSPACE_INSTALL_MC=${WORKSPACE_INSTALL_MC}
WORKSPACE_INSTALL_SYMFONY=${WORKSPACE_INSTALL_SYMFONY}
WORKSPACE_INSTALL_PYTHON=${WORKSPACE_INSTALL_PYTHON}
WORKSPACE_INSTALL_PYTHON3=${WORKSPACE_INSTALL_PYTHON3}
WORKSPACE_INSTALL_POWERLINE=${WORKSPACE_INSTALL_POWERLINE}
WORKSPACE_INSTALL_SUPERVISOR=${WORKSPACE_INSTALL_SUPERVISOR}
WORKSPACE_INSTALL_IMAGE_OPTIMIZERS=${WORKSPACE_INSTALL_IMAGE_OPTIMIZERS}
WORKSPACE_INSTALL_IMAGEMAGICK=${WORKSPACE_INSTALL_IMAGEMAGICK}
WORKSPACE_IMAGEMAGICK_VERSION=${WORKSPACE_IMAGEMAGICK_VERSION}
WORKSPACE_INSTALL_TERRAFORM=${WORKSPACE_INSTALL_TERRAFORM}
WORKSPACE_INSTALL_DUSK_DEPS=${WORKSPACE_INSTALL_DUSK_DEPS}
WORKSPACE_INSTALL_PG_CLIENT=${WORKSPACE_INSTALL_PG_CLIENT}
WORKSPACE_INSTALL_PHALCON=${WORKSPACE_INSTALL_PHALCON}
WORKSPACE_INSTALL_SWOOLE=${WORKSPACE_INSTALL_SWOOLE}
WORKSPACE_INSTALL_TAINT=${WORKSPACE_INSTALL_TAINT}
WORKSPACE_INSTALL_LIBPNG=${WORKSPACE_INSTALL_LIBPNG}
WORKSPACE_INSTALL_GRAPHVIZ=${WORKSPACE_INSTALL_GRAPHVIZ}
WORKSPACE_INSTALL_IONCUBE=${WORKSPACE_INSTALL_IONCUBE}
WORKSPACE_INSTALL_MYSQL_CLIENT=${WORKSPACE_INSTALL_MYSQL_CLIENT}
WORKSPACE_INSTALL_PING=${WORKSPACE_INSTALL_PING}
WORKSPACE_INSTALL_SSHPASS=${WORKSPACE_INSTALL_SSHPASS}
WORKSPACE_INSTALL_INOTIFY=${WORKSPACE_INSTALL_INOTIFY}
WORKSPACE_INSTALL_FSWATCH=${WORKSPACE_INSTALL_FSWATCH}
WORKSPACE_INSTALL_YAML=${WORKSPACE_INSTALL_YAML}
WORKSPACE_INSTALL_RDKAFKA=${WORKSPACE_INSTALL_RDKAFKA}
WORKSPACE_INSTALL_MAILPARSE=${WORKSPACE_INSTALL_MAILPARSE}
WORKSPACE_INSTALL_XMLRPC=${WORKSPACE_INSTALL_XMLRPC}
WORKSPACE_PUID=${WORKSPACE_PUID}
WORKSPACE_PGID=${WORKSPACE_PGID}
WORKSPACE_CHROME_DRIVER_VERSION=${WORKSPACE_CHROME_DRIVER_VERSION}
WORKSPACE_TIMEZONE=${WORKSPACE_TIMEZONE}
WORKSPACE_SSH_PORT=${WORKSPACE_SSH_PORT}
WORKSPACE_INSTALL_FFMPEG=${WORKSPACE_INSTALL_FFMPEG}
WORKSPACE_INSTALL_AUDIOWAVEFORM=${WORKSPACE_INSTALL_AUDIOWAVEFORM}
WORKSPACE_INSTALL_WKHTMLTOPDF=${WORKSPACE_INSTALL_WKHTMLTOPDF}
WORKSPACE_INSTALL_GNU_PARALLEL=${WORKSPACE_INSTALL_GNU_PARALLEL}
WORKSPACE_INSTALL_AST=${WORKSPACE_INSTALL_AST}
WORKSPACE_AST_VERSION=${WORKSPACE_AST_VERSION}
WORKSPACE_BROWSERSYNC_HOST_PORT=${WORKSPACE_BROWSERSYNC_HOST_PORT}
WORKSPACE_BROWSERSYNC_UI_HOST_PORT=${WORKSPACE_BROWSERSYNC_UI_HOST_PORT}
WORKSPACE_VUE_CLI_SERVE_HOST_PORT=${WORKSPACE_VUE_CLI_SERVE_HOST_PORT}
WORKSPACE_VUE_CLI_UI_HOST_PORT=${WORKSPACE_VUE_CLI_UI_HOST_PORT}
WORKSPACE_ANGULAR_CLI_SERVE_HOST_PORT=${WORKSPACE_ANGULAR_CLI_SERVE_HOST_PORT}
WORKSPACE_INSTALL_GIT_PROMPT=${WORKSPACE_INSTALL_GIT_PROMPT}
WORKSPACE_INSTALL_DOCKER_CLIENT=${WORKSPACE_INSTALL_DOCKER_CLIENT}
WORKSPACE_INSTALL_LNAV=${WORKSPACE_INSTALL_LNAV}
WORKSPACE_INSTALL_PROTOC=${WORKSPACE_INSTALL_PROTOC}
WORKSPACE_INSTALL_PHPDECIMAL=${WORKSPACE_INSTALL_PHPDECIMAL}
WORKSPACE_INSTALL_ZOOKEEPER=${WORKSPACE_INSTALL_ZOOKEEPER}
WORKSPACE_PROTOC_VERSION=${WORKSPACE_PROTOC_VERSION}
WORKSPACE_INSTALL_MEMCACHED=${WORKSPACE_INSTALL_MEMCACHED}

### PHP_FPM ###############################################

PHP_FPM_BASE_IMAGE_TAG_PREFIX=${PHP_FPM_BASE_IMAGE_TAG_PREFIX}
PHP_FPM_INSTALL_BCMATH=${PHP_FPM_INSTALL_BCMATH}
PHP_FPM_INSTALL_MYSQLI=${PHP_FPM_INSTALL_MYSQLI}
PHP_FPM_INSTALL_INTL=${PHP_FPM_INSTALL_INTL}
PHP_FPM_INSTALL_IMAGEMAGICK=${PHP_FPM_INSTALL_IMAGEMAGICK}
PHP_FPM_IMAGEMAGICK_VERSION=${PHP_FPM_IMAGEMAGICK_VERSION}
PHP_FPM_INSTALL_OPCACHE=${PHP_FPM_INSTALL_OPCACHE}
PHP_FPM_INSTALL_IMAGE_OPTIMIZERS=${PHP_FPM_INSTALL_IMAGE_OPTIMIZERS}
PHP_FPM_INSTALL_PHPREDIS=${PHP_FPM_INSTALL_PHPREDIS}
PHP_FPM_INSTALL_MEMCACHED=${PHP_FPM_INSTALL_MEMCACHED}
PHP_FPM_INSTALL_BZ2=${PHP_FPM_INSTALL_BZ2}
PHP_FPM_INSTALL_ENCHANT=${PHP_FPM_INSTALL_ENCHANT}
PHP_FPM_INSTALL_GMP=${PHP_FPM_INSTALL_GMP}
PHP_FPM_INSTALL_GNUPG=${PHP_FPM_INSTALL_GNUPG}
PHP_FPM_INSTALL_XDEBUG=${PHP_FPM_INSTALL_XDEBUG}
PHP_FPM_INSTALL_PCOV=${PHP_FPM_INSTALL_PCOV}
PHP_FPM_INSTALL_XHPROF=${PHP_FPM_INSTALL_XHPROF}
PHP_FPM_INSTALL_PHPDBG=${PHP_FPM_INSTALL_PHPDBG}
PHP_FPM_INSTALL_SMB=${PHP_FPM_INSTALL_SMB}
PHP_FPM_INSTALL_IMAP=${PHP_FPM_INSTALL_IMAP}
PHP_FPM_INSTALL_MONGO=${PHP_FPM_INSTALL_MONGO}
PHP_FPM_INSTALL_AMQP=${PHP_FPM_INSTALL_AMQP}
PHP_FPM_INSTALL_CASSANDRA=${PHP_FPM_INSTALL_CASSANDRA}
PHP_FPM_INSTALL_GEARMAN=${PHP_FPM_INSTALL_GEARMAN}
PHP_FPM_INSTALL_MSSQL=${PHP_FPM_INSTALL_MSSQL}
PHP_FPM_INSTALL_SSH2=${PHP_FPM_INSTALL_SSH2}
PHP_FPM_INSTALL_SOAP=${PHP_FPM_INSTALL_SOAP}
PHP_FPM_INSTALL_XSL=${PHP_FPM_INSTALL_XSL}
PHP_FPM_INSTALL_EXIF=${PHP_FPM_INSTALL_EXIF}
PHP_FPM_INSTALL_AEROSPIKE=${PHP_FPM_INSTALL_AEROSPIKE}
PHP_FPM_INSTALL_OCI8=${PHP_FPM_INSTALL_OCI8}
PHP_FPM_INSTALL_PGSQL=${PHP_FPM_INSTALL_PGSQL}
PHP_FPM_INSTALL_GHOSTSCRIPT=${PHP_FPM_INSTALL_GHOSTSCRIPT}
PHP_FPM_INSTALL_LDAP=${PHP_FPM_INSTALL_LDAP}
PHP_FPM_INSTALL_PHALCON=${PHP_FPM_INSTALL_PHALCON}
PHP_FPM_INSTALL_SWOOLE=${PHP_FPM_INSTALL_SWOOLE}
PHP_FPM_INSTALL_TAINT=${PHP_FPM_INSTALL_TAINT}
PHP_FPM_INSTALL_PG_CLIENT=${PHP_FPM_INSTALL_PG_CLIENT}
PHP_FPM_INSTALL_POSTGIS=${PHP_FPM_INSTALL_POSTGIS}
PHP_FPM_INSTALL_PCNTL=${PHP_FPM_INSTALL_PCNTL}
PHP_FPM_INSTALL_CALENDAR=${PHP_FPM_INSTALL_CALENDAR}
PHP_FPM_INSTALL_FAKETIME=${PHP_FPM_INSTALL_FAKETIME}
PHP_FPM_INSTALL_IONCUBE=${PHP_FPM_INSTALL_IONCUBE}
PHP_FPM_INSTALL_RDKAFKA=${PHP_FPM_INSTALL_RDKAFKA}
PHP_FPM_INSTALL_GETTEXT=${PHP_FPM_INSTALL_GETTEXT}
PHP_FPM_INSTALL_XMLRPC=${PHP_FPM_INSTALL_XMLRPC}
PHP_FPM_FAKETIME=${PHP_FPM_FAKETIME}
PHP_FPM_INSTALL_APCU=${PHP_FPM_INSTALL_APCU}
PHP_FPM_INSTALL_CACHETOOL=${PHP_FPM_INSTALL_CACHETOOL}
PHP_FPM_INSTALL_YAML=${PHP_FPM_INSTALL_YAML}
PHP_FPM_INSTALL_ADDITIONAL_LOCALES=${PHP_FPM_INSTALL_ADDITIONAL_LOCALES}
PHP_FPM_INSTALL_MYSQL_CLIENT=${PHP_FPM_INSTALL_MYSQL_CLIENT}
PHP_FPM_INSTALL_PING=${PHP_FPM_INSTALL_PING}
PHP_FPM_INSTALL_SSHPASS=${PHP_FPM_INSTALL_SSHPASS}
PHP_FPM_INSTALL_MAILPARSE=${PHP_FPM_INSTALL_MAILPARSE}
PHP_FPM_INSTALL_WKHTMLTOPDF=${PHP_FPM_INSTALL_WKHTMLTOPDF}
PHP_FPM_INSTALL_PHPDECIMAL=${PHP_FPM_INSTALL_PHPDECIMAL}
PHP_FPM_INSTALL_ZOOKEEPER=${PHP_FPM_INSTALL_ZOOKEEPER}
PHP_FPM_FFMPEG=${PHP_FPM_FFMPEG}
PHP_FPM_AUDIOWAVEFORM=${PHP_FPM_AUDIOWAVEFORM}
PHP_FPM_ADDITIONAL_LOCALES=${PHP_FPM_ADDITIONAL_LOCALES}
PHP_FPM_INSTALL_DOCKER_CLIENT=${PHP_FPM_INSTALL_DOCKER_CLIENT}
PHP_FPM_DEFAULT_LOCALE=${PHP_FPM_DEFAULT_LOCALE}
PHP_FPM_XDEBUG_PORT=${PHP_FPM_XDEBUG_PORT}

PHP_FPM_PUID=${PHP_FPM_PUID}
PHP_FPM_PGID=${PHP_FPM_PGID}

### PHP_FPM_NEW_RELIC #####################################

PHP_FPM_NEW_RELIC=${PHP_FPM_NEW_RELIC}
PHP_FPM_NEW_RELIC_KEY=${PHP_FPM_NEW_RELIC_KEY}
PHP_FPM_NEW_RELIC_APP_NAME=${PHP_FPM_NEW_RELIC_APP_NAME}

### NGINX #################################################

NGINX_HOST_HTTP_PORT=${NGINX_HOST_HTTP_PORT}
NGINX_HOST_HTTPS_PORT=${NGINX_HOST_HTTPS_PORT}
NGINX_HOST_LOG_PATH=${NGINX_HOST_LOG_PATH}
NGINX_SITES_PATH=${NGINX_SITES_PATH}
NGINX_PHP_UPSTREAM_CONTAINER=${NGINX_PHP_UPSTREAM_CONTAINER}
NGINX_PHP_UPSTREAM_PORT=${NGINX_PHP_UPSTREAM_PORT}
NGINX_SSL_PATH=${NGINX_SSL_PATH]

#INSTALL_BLACKFIRE=false
#BLACKFIRE_CLIENT_ID=
#BLACKFIRE_CLIENT_TOKEN=

### MYSQL #################################################

MYSQL_VERSION=${MYSQL_VERSION}
MYSQL_DATABASE=${MYSQL_DATABASE}
MYSQL_USER=${MYSQL_USER}
MYSQL_PASSWORD=${MYSQL_PASSWORD}
MYSQL_PORT=${MYSQL_PORT}
MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
MYSQL_ENTRYPOINT_INITDB=${MYSQL_ENTRYPOINT_INITDB}


### PHP_WORKER ############################################

PHP_WORKER_INSTALL_BZ2=false
PHP_WORKER_INSTALL_GD=false
PHP_WORKER_INSTALL_IMAGEMAGICK=false
PHP_WORKER_IMAGEMAGICK_VERSION=latest
PHP_WORKER_INSTALL_GMP=false
PHP_WORKER_INSTALL_GNUPG=false
PHP_WORKER_INSTALL_LDAP=false
PHP_WORKER_INSTALL_PGSQL=false
PHP_WORKER_INSTALL_MONGO=false
PHP_WORKER_INSTALL_BCMATH=false
PHP_WORKER_INSTALL_MEMCACHED=false
# PHP_WORKER_INSTALL_OCI8 Does not work in php5.6 version
PHP_WORKER_INSTALL_OCI8=false
PHP_WORKER_INSTALL_PHALCON=false
PHP_WORKER_INSTALL_SOAP=false
PHP_WORKER_INSTALL_ZIP_ARCHIVE=false
PHP_WORKER_INSTALL_MYSQL_CLIENT=false
PHP_WORKER_INSTALL_AMQP=false
PHP_WORKER_INSTALL_GHOSTSCRIPT=false
PHP_WORKER_INSTALL_SWOOLE=false
PHP_WORKER_INSTALL_TAINT=false
PHP_WORKER_INSTALL_FFMPEG=false
PHP_WORKER_INSTALL_AUDIOWAVEFORM=false
PHP_WORKER_INSTALL_CASSANDRA=false
PHP_WORKER_INSTALL_GEARMAN=false
PHP_WORKER_INSTALL_REDIS=false
PHP_WORKER_INSTALL_IMAP=false
PHP_WORKER_INSTALL_XMLRPC=false

PHP_WORKER_PUID=1000
PHP_WORKER_PGID=1000

